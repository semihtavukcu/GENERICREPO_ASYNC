
# Desc
Generic Repository Pattern with EF Core in .NET 6 
# Features
- JWT & Identity Implementation
- One to Many Relationship Entities
- File upload implementation
